import argparse
from completer import Completer

ap = argparse.ArgumentParser()

ap.add_argument("-f", "--file", required=True, help="The input CSV file")
ap.add_argument("-o", "--output", required=False, help="The output CSV file")

args = vars(ap.parse_args())

input_file = args["file"]
output_file = args["output"]

if args["output"] is None:
    output_file = input_file

Completer(input_file, output_file).process()
