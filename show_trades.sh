#!/bin/bash

header=`head -1 $1`
formated=`awk -F, 'NR>=2 {printf("%s,%s,%s,%.4f,%.4f,%.4f,%.4f,%.4f\n",$1,$2,$3,$4,$5,$6,$7,$8)}' $1`

printf '%s\n%s\n' "$header" "$formated" | column -s, -t
