from panwid import ScrollingListBox
from urwid_utils import PaletteEntry, Palette


def palette():
    entries = ScrollingListBox.get_palette_entries()
    entries["white inverse"] = PaletteEntry(
        mono="black",
        foreground="black",
        background="white"
    )
    entries["light blue"] = PaletteEntry(
        mono="white",
        foreground="light blue",
        background="black"
    )
    entries["dark blue"] = PaletteEntry(
        mono="white",
        foreground="dark blue",
        background="black"
    )
    return Palette("default", **entries)
