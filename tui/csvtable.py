from panwid import DataTableColumn, DataTable
import csv

from hscrollbar import HScrollBar


class CsvTable(DataTable):

    def __init__(self, file_path):
        self.file_path = file_path
        self.with_scrollbar = HScrollBar
        self.reload()

        super().__init__()

    def reload(self):
        csv_headers, csv_data = self.load_file()
        self.columns = list(map(lambda title: DataTableColumn(title), csv_headers))
        self.data = csv_data

    def load_file(self):
        with open(self.file_path, 'r') as f:
            csv_reader = csv.reader(f)

            csv_headers = next(csv_reader)
            csv_data = self.__list_to_dict(csv_headers, list(csv_reader))

            return csv_headers, csv_data

    @staticmethod
    def __list_to_dict(keys, rows):
        result = []
        for row in rows:
            current_row = {}
            for key_idx in range(0, len(keys)):
                key_name = keys[key_idx]
                current_row[key_name] = CsvTable.__process_cell_value(row[key_idx])
            result.append(current_row)

        return result

    @staticmethod
    def __process_cell_value(value):
        try:
            return "{:.4f}".format(float(value))
        except ValueError:
            return value
