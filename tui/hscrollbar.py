from panwid import ScrollBar


class HScrollBar(ScrollBar):

    _thumb_char = ("light blue", "\u2588")
    _trough_char = ("dark blue", "\u2591")
    _thumb_indicator_top = ("white inverse", "\u234d")
    _thumb_indicator_bottom = ("white inverse", "\u2354")
