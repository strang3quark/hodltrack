import urwid

from csvtable import CsvTable
import pallete

def unhandled_input(key):
    if key in ("q", "Q"):
        raise urwid.ExitMainLoop()


class App(object):

    def __init__(self, file_path) -> None:
        super().__init__()

        self.table = CsvTable(file_path)
        header = urwid.Text('HodlTrack')
        footer = urwid.Text('Portfolio Value: 3003020')

        screen = urwid.raw_display.Screen()
        cols, rows = screen.get_cols_rows()

        master_pile = urwid.Pile([
            header,
            urwid.Divider(u'─'),
            urwid.BoxAdapter(self.table, rows - 4),
            urwid.Divider(u'─'),
            footer,
        ])

        w = urwid.Filler(master_pile, 'top')
        loop = urwid.MainLoop(
            w,
            palette=pallete.palette(),
            unhandled_input=unhandled_input
        )

        loop.run()
