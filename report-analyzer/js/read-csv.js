function handleFiles(files) {
	if (window.FileReader) {
		getAsText(files[0]);
	} else {
		alert('FileReader are not supported in this browser.');
	}
}

function getAsText(fileToRead) {
	const reader = new FileReader();
	// Handle errors load
	reader.onload = loadHandler;
	reader.onerror = errorHandler;
	// Read file into memory as UTF-8
	reader.readAsText(fileToRead);
}

function loadHandler(event) {
	const csv = event.target.result;
	processDataAsObj(csv);
}

function processDataAsObj(csv) {
	const allTextLines = csv.split(/\r\n|\n/)
		.filter(line => !!line)

	const headers = allTextLines.shift().split(',');

	const entries = allTextLines.map(line => {
		const arr = line.split(',');
		return {
			date: arr[0],
			coin: arr[1],
			size: parseFloat(arr[2]),
			coinValue: parseFloat(arr[3]),
			investedFiat: parseFloat(arr[4]),
			sizeSum: parseFloat(arr[5]),
			investedFiatSum: parseFloat(arr[6]),
			walletValue: parseFloat(arr[7])
		}
	});

	drawTable(headers, entries);
	drawChart(entries)
}

function errorHandler(evt) {
	if (evt.target.error.name == "NotReadableError") {
		alert("Canno't read file !");
	}
}

//draw the table, if first line contains heading
function drawTable(headers, entries) {
	//Clear previous data
	document.getElementById("table-output").innerHTML = "";
	const table = document.createElement("table");

	//for the table headings
	const tableHeader = table.insertRow(-1);

	headers.map(header => {
		const el = document.createElement("th");
		el.innerText = header;
		return el;
	}).forEach(th => tableHeader.appendChild(th));


	//the data
	entries
		.map(entry => {
			return [
				createCell(entry.date),
				createCell(entry.coin),
				createCell(entry.size),
				createCell(entry.coinValue.toFixed(4)),
				createCell(entry.investedFiat.toFixed(4)),
				createCell(entry.sizeSum.toFixed(4)),
				createCell(entry.investedFiatSum.toFixed(4)),
				createCell(
					entry.walletValue.toFixed(4),
					entry.walletValue >= entry.investedFiatSum ? 'green' : 'red'
				)
			];
		})
		.forEach(row => {
			const domRow = table.insertRow(-1);
			row.forEach(cell => domRow.insertCell(-1).appendChild(cell));
		});

	document.getElementById("table-output").appendChild(table);
}

function createCell(value, color) {
	let element;

	if (color) {
		element = document.createElement('span');
		element.setAttribute('style', 'color: ' + color);
		element.innerText = value;
	} else {
		element = document.createTextNode(value);
	}

	return element
}

// draw the chart
function drawChart(entries) {

	const invested = entries.map(entry => entry.investedFiatSum)
	const value = entries.map(entry => entry.walletValue)
	const xValues = entries.map(entry => entry.date)

	new Chart("chart", {
		type: "line",
		data: {
			labels: xValues,
			datasets: [{
				label: "Invested",
				data: invested,
				borderColor: "red",
				fill: false
			}, {
				label: "Value",
				data: value,
				borderColor: "green",
				fill: false
			}]
		},
		options: {
			legend: { display: true }
		}
	});
}