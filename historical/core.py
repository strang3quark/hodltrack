#!/usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = 'Rohit Gupta'

"""
Cryptocurrency price History from coinmarketcap.com
"""

from __future__ import print_function

__all__ = ["CmcScraper"]

from datetime import datetime
from .utils import download_coin_data, InvalidParameters


class CmcScraper(object):
    """
    Scrape cryptocurrency historical market price data from coinmarketcap.com
    """

    def __init__(
            self,
            coin_code,
            currency="EUR",
            start_date=None,
            end_date=None,
            all_time=False,
            order_ascending=False,
    ):
        """
        :param coin_code: coin code of cryptocurrency e.g. btc
        :param currency: the currency to compare against the cryptocurrency e.g. eur
        :param start_date: date since when to scrape data (in the format of dd-mm-yyyy)
        :param end_date: date to which scrape the data (in the format of dd-mm-yyyy)
        :param all_time: 'True' if need data of all time for respective cryptocurrency
        :param order_ascending: data ordered by 'Date' in ascending order (i.e. oldest first).
        """

        self.coin_code = coin_code
        self.currency = currency
        self.start_date = start_date
        self.end_date = end_date
        self.all_time = bool(all_time)
        self.order_ascending = order_ascending
        self.headers = ["Date", "Open", "High", "Low", "Close", "Volume", "Market Cap"]
        self.rows = []

        # enable all_time download if start_time or end_time is not given
        if not (self.start_date and self.end_date):
            self.all_time = True

        if not (self.all_time or (self.start_date and self.end_date)):
            raise InvalidParameters(
                "'start_date' or 'end_date' cannot be empty if 'all_time' flag is False"
            )

    def __repr__(self):
        return (
            "<historical coin_code:{}, start_date:{}, end_date:{}, all_time:{}>".format(
                self.coin_code, self.start_date, self.end_date, self.all_time
            )
        )

    def _download_data(self, **kwargs):
        """
        This method downloads the data.
        :param forced: (optional) if ``True``, data will be re-downloaded.
        :return:
        """

        forced = kwargs.get("forced")

        if self.rows and not forced:
            return

        if self.all_time:
            self.start_date, self.end_date = None, None

        coin_data = download_coin_data(self.coin_code, self.currency, self.start_date, self.end_date)

        for _row in coin_data["data"]["quotes"]:
            _row_quote = list(_row["quote"].values())[0]
            date = datetime.strptime(
                _row_quote["timestamp"], "%Y-%m-%dT%H:%M:%S.%fZ"
            ).strftime("%d-%m-%Y")

            row = [
                date,
                _row_quote["open"],
                _row_quote["high"],
                _row_quote["low"],
                _row_quote["close"],
                _row_quote["volume"],
                _row_quote["market_cap"],
            ]

            self.rows.insert(0, row)

        self.end_date, self.start_date = self.rows[0][0], self.rows[-1][0]

        if self.order_ascending:
            self.rows.sort(key=lambda x: datetime.strptime(x[0], "%d-%m-%Y"))

    def get_data(self, **kwargs):
        """
        :param kwargs: Optional arguments that data downloader takes.
        :return:
        """
        self._download_data(**kwargs)
        return self.headers, self.rows
