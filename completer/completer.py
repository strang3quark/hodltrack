from csv import reader, writer
from historical import CmcScraper

__all__ = ["Completer"]


class Completer(object):

    def __init__(self,
                 origin_file,
                 destination_file,
                 currency="EUR") -> None:

        self.origin_file = origin_file
        self.destination_file = destination_file
        self.currency = currency

    def get_value_at_day(self, coin, day):
        scraper = CmcScraper(coin, currency=self.currency, start_date=day, end_date=day)
        headers, data = scraper.get_data()
        return float(data[0][4])

    def get_complete_trade_info(self):
        result = []

        with open(self.origin_file, 'r') as f:
            csv_reader = reader(f)
            header = next(csv_reader)

            result.append(header)

            size_sum = 0
            invested_sum = 0

            if header is not None:
                for row in csv_reader:

                    day_str = row[0]
                    coin = row[1]
                    size_str = row[2]

                    coin_value = None
                    if len(row) < 4:
                        coin_value = self.get_value_at_day(coin, day_str)
                        row.append(str(coin_value))
                    else:
                        coin_value = float(row[3])

                    if len(row) < 7:
                        size = float(size_str)
                        invested = size * coin_value

                        size_sum += size
                        invested_sum += invested

                        row.append(str(invested))
                        row.append(str(size_sum))
                        row.append(str(invested_sum))
                        row.append(str(size_sum * coin_value))
                    else:
                        size = float(size_str)

                        invested = size * coin_value

                        size_sum += size
                        invested_sum += invested

                    result.append(row)

        return result

    def write_trade_info(self, rows):
        with open(self.destination_file, 'w') as f:
            csv_writer = writer(f)
            csv_writer.writerows(rows)

    def process(self):
        completed_trades = self.get_complete_trade_info()
        self.write_trade_info(completed_trades)
